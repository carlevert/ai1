/**
 * @author Carl-Evert Kangas <dv14cks@cs.umu.se>
 * @author Stina Olofsson <id13son@cs.umu.se>
 */
import com.fasterxml.jackson.annotation.JsonProperty;

public class P {

    @JsonProperty("Pose")
    private Pose pose;

    @JsonProperty("Status")
    private int status;

    @JsonProperty("Timestamp")
    private long timestamp;

    public Pose getPose() {
	return pose;
    }

    public void setPose(Pose p) {
	pose = p;
    }

    public int getStatus() {
	return status;
    }

    public void setStatus(int s) {
	status = s;
    }

    public long getTimestamp() {
	return timestamp;
    }

    public void setTimestamp(long t) {
	timestamp = t;
    }

    public class Pose {

	@JsonProperty("Orientation")
	private Orientation orientation;

	@JsonProperty("Position")
	private Position position;

	public Orientation getOrientation() {
	    return orientation;
	}

	public void setOrientation(Orientation o) {
	    orientation = o;
	}

	public Position getPosition() {
	    return position;
	}

	public void setPosition(Position p) {
	    position = p;
	}

	public class Orientation {
	    @JsonProperty("W")
	    private double _W;

	    @JsonProperty("X")
	    private double _X;

	    @JsonProperty("Y")
	    private double _Y;

	    @JsonProperty("Z")
	    private double _Z;

	    public double getW() {
		return _W;
	    }

	    public double getX() {
		return _X;
	    }

	    public double getY() {
		return _Y;
	    }

	    public double getZ() {
		return _Z;
	    }

	    public void setW(double w) {
		_W = w;
	    }

	    public void setX(double x) {
		_X = x;
	    }

	    public void setY(double y) {
		_Y = y;
	    }

	    public void setZ(double z) {
		_Z = z;
	    }

	}

	public class Position {
	    @JsonProperty("X")
	    private double _X;
	    @JsonProperty("Y")
	    private double _Y;
	    @JsonProperty("Z")
	    private double _Z;

	    public double getX() {
		return _X;
	    }

	    public double getY() {
		return _Y;
	    }

	    public double getZ() {
		return _Z;
	    }

	    public void setX(double x) {
		_X = x;
	    }

	    public void setY(double x) {
		_Y = x;
	    }

	    public void setZ(double x) {
		_Z = x;
	    }

	    public String toString() {
		return String.format("(%.4f, %.4f, %.4 f)", _X, _Y, _Z);
	    }

	}

    }

}
