/**
 * @author Carl-Evert Kangas <dv14cks@cs.umu.se>
 * @author Stina Olofsson <id13son@cs.umu.se>
 * @author thomasj
 */

import java.text.SimpleDateFormat;

import javax.swing.JFileChooser;

public class Pilot {

    private RobotCommunication robotcomm; // communication drivers
    private Path path;

    
    /**
     * Create a robot connected to host "host" at port "port"
     * 
     * @param host
     *            normally http://127.0.0.1
     * @param port
     *            normally 50000
     */
    public Pilot(String host, int port) {
	robotcomm = new RobotCommunication(host, port);
    }

    private String selectPathFile() {
	JFileChooser fileChooser = new JFileChooser();
	int f = fileChooser.showOpenDialog(null);
	if (f == JFileChooser.APPROVE_OPTION)
	    return fileChooser.getSelectedFile().getAbsolutePath();
	else
	    return null;
    }

    public void readPath() {
	path = new Path();
	path.fromFile(selectPathFile());
    }

    /**
     * Main-method for path-following robot control application.
     * 
     * @param args
     *            not used
     * @throws Exception
     *             not caught
     */
    public static void main(String[] args) throws Exception {
	Pilot pilot = new Pilot("http://127.0.0.1", 50000);
	pilot.readPath();
	pilot.drive();
    }

    /**
     * Controls the robot via RobotCommunication interface and follow-the-carrot
     * path-tracking-algorithm.
     * 
     * @throws Exception not caught
     */
    private void drive() throws Exception {

	LocalizationResponse lr = new LocalizationResponse();
	DifferentialDriveRequest dr = new DifferentialDriveRequest();

	/* Fixed radius in which robot can discover points on path */
	final double lookaheadDistance = 1.125;

	/* Factor by which steering/turning intent is amplified */
	final double gain = 2.0;

	/* angularSpeed unit is rad/s, start with 0 */
	double angularSpeed = 0.0;

	/* Max linearSpeed is 1, full throttle */
	double linearSpeed = 1.0;

	/* How many ms to drive at current speeds before recalculating */
	final long sleep = 100;
	
	/* Last Position that defines path */
	Position goalPosition = path.getLastPosition();

	long startTime = System.currentTimeMillis();
	String formatString = "HH:mm:ss.SSS";
	System.out.println("Timer started at " +
		new SimpleDateFormat(formatString).format(startTime));
	long stopTime;
	long time;
	
	
	boolean done = false;
	while (!done) {

	    /* Query robot for pose and position */
	    robotcomm.getResponse(lr);
	    double heading = lr.getHeadingAngle();

	    /* Get robot position from response and point at lookahead distance */
	    double[] robotPosition = getPosition(lr);
	    double[] pointAtLookaheadDist = path
		    .getPositionAtLookaheadDistance(robotPosition,
			    lookaheadDistance);

	    /* Calculate bearing */
	    double bearing = Math.atan2(pointAtLookaheadDist[1]
		    - robotPosition[1], pointAtLookaheadDist[0]
		    - robotPosition[0]);

	    /*
	     * Angular speed is set to the orientation error. The orientation
	     * error is kept in range -Pi to Pi
	     */
	    angularSpeed = bearing - heading;
	    if (angularSpeed > Math.PI)
		angularSpeed = (2 * Math.PI - angularSpeed) * -1;
	    else if (angularSpeed < -1 * Math.PI)
		angularSpeed = 2 * Math.PI + angularSpeed;

	    /* Robot is less than 1.0m from last point on path */
	    if (goalPosition.getDistanceTo(new Position(robotPosition)) < 1.0
		    && path.indexOnLastPosition()) {
		stopTime = System.currentTimeMillis();
		System.out.println("Timer stopped at " +
				new SimpleDateFormat(formatString).format(stopTime));
		time = stopTime - startTime;
		long seconds = time / 1000;
		long minutes = time / (60 * 1000) % 60;
		long millis = time % 1000;
		System.out.println("Path completed in " + minutes + "."
			+ seconds + "." + millis);
		done = true;
		continue;
	    }

	    dr.setLinearSpeed(linearSpeed);
	    dr.setAngularSpeed(angularSpeed * gain);

	    robotcomm.putRequest(dr);

	    Thread.sleep(sleep);
	}

	dr.setLinearSpeed(0);
	dr.setAngularSpeed(0);

	robotcomm.putRequest(dr);
    }

    /**
     * Extract the robot bearing from the response
     * 
     * @param lr
     * @return angle in degrees
     */
    double getBearingAngle(LocalizationResponse lr) {
	double e[] = lr.getOrientation();

	double angle = 2 * Math.atan2(e[3], e[0]);
	return angle * 180 / Math.PI;
    }

    /**
     * Extract the position
     * 
     * @param lr
     * @return coordinates
     */
    double[] getPosition(LocalizationResponse lr) {
	return lr.getPosition();
    }


}
