

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Carl-Evert Kangas <dv14cks@cs.umu.se>
 * @author Stina Olofsson <id13son@cs.umu.se>
 */
public class Path {

    private List<Position> path;

    /**
     * Path will have at least threshold distance between nodes.
     */
    private double threshold = 0.1;

    /**
     * Cursor that keeps track of last carrot point.
     */
    private int currentIndex = 0;

    public List<Position> getPath() {
	return path;
    }

    public void fromFile(String filename) {
	this.path = new ArrayList<Position>();
	ObjectMapper objectMapper = new ObjectMapper();
	File file = new File(filename);
	if (!file.exists())
	    System.err.println("File " + filename + " does not exist.");
	TypeReference<List<P>> typeReference = new TypeReference<List<P>>() {
	};
	try {
	    List<P> tempPath = objectMapper.readValue(file, typeReference);
	    Position prev = null;
	    /*
	     * Add Positions to path only if distance to previous Position is
	     * greater than threshold.
	     */
	    for (P p : tempPath) {
		double x = p.getPose().getPosition().getX();
		double y = p.getPose().getPosition().getY();
		Position newPosition = new Position(x, y);
		if (prev == null) {
		    prev = newPosition;
		    path.add(newPosition);
		} else {
		    if (newPosition.getDistanceTo(prev) > threshold) {
			prev = newPosition;
			path.add(newPosition);
		    }
		}
	    }
	} catch (JsonParseException e) {
	    e.printStackTrace();
	} catch (JsonMappingException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    /**
     * Finds point to aim for within given lookahead distance of the robot.
     * Avoids picking up already passed nodes from the beginning of the path as
     * well as picking up nodes from the end of the path by using a cursor to
     * the current aiming point.
     * 
     * @param robotPosition
     *            Robot position as an array of two doubles.
     * @param lookaheadDistance
     *            The lookahead distance
     * @return Position (as an array of doubles) of the last Position (object)
     *         within the lookahead distance of the robot.
     */
    public double[] getPositionAtLookaheadDistance(double[] robotPosition,
	    double lookaheadDistance) {
	while (path.get(currentIndex)
		.getDistanceTo(new Position(robotPosition)) < lookaheadDistance) {
	    if (currentIndex < path.size() - 1)
		currentIndex++;
	    else
		break;
	}
	double[] ret = new double[2];
	Position pos = path.get(currentIndex);
	ret[0] = pos.getX();
	ret[1] = pos.getY();
	return ret;
    }

    /**
     * @return Last position on path.
     */
    public Position getLastPosition() {
	return path.get(path.size() - 1);
    }

    /**
     * @return true if index has advanced to the last Position on path.
     */
    public boolean indexOnLastPosition() {
	return currentIndex == (path.size() - 1);
    }

}